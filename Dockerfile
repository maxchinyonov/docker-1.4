FROM debian:9 as build
RUN apt update && apt install -y wget tar gcc libpcre3-dev zlib1g-dev make
RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && cd nginx-1.0.5 && ./configure && make && make install && cd /usr/local/nginx/sbin && ./nginx

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY --from=build /usr/local/nginx/conf/mime.types ../conf/mime.types
COPY --from=build /usr/local/nginx/html/index.html ../html/index.html
RUN mkdir ../logs && touch ../logs/error.log && chmod +x nginx
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]
