<h1 class="code-line" data-line-start=0 data-line-end=1 ><a id="__0"></a>Описание задания</h1>
<h4 class="code-line" data-line-start=2 data-line-end=3 ><a id="C_nginx____________2"></a>Cобрать nginx из исходников со стандартными модулями и запустить бинарник во втором образе.</h4>
<p class="has-line-data" data-line-start="4" data-line-end="10"><a href="https://habr.com/ru/post/321468/">https://habr.com/ru/post/321468/</a><br>
Образы Debian9<br>
При запуске примаунтить конфиг в контейнер.<br>
<a href="https://www.thegeekstuff.com/2011/07/install-nginx-from-source/">https://www.thegeekstuff.com/2011/07/install-nginx-from-source/</a><br>
<a href="https://docs.docker.com/develop/develop-images/multistage-build/">https://docs.docker.com/develop/develop-images/multistage-build/</a><br>
<a href="https://docs.docker.com/storage/bind-mounts/">https://docs.docker.com/storage/bind-mounts/</a></p>
