
docker build . -t nginxcustom:1

docker run -d -p 80:80 \
--mount type=bind,source="$(pwd)"/nginx.conf,target=/usr/local/nginx/conf/nginx.conf \
nginxcustom:1
